<?php

/* @var $this yii\web\View */

$this->title = 'MindGeek Interview';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>MindGeek Test</h1>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Sources</h2>

                <p>One of the functionalities in this project is the possibility to add multiple sources.</p>
                <p>These sources will be processed and downloaded</p>

                <p><a class="btn btn-default" href="/sources">Manage sources</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Run unprocessed sources</h2>

                <p>We might have unprocessed sources. This functionality will download these sources</p>

                <p><a class="btn btn-default" href="/sources/run">Run sources</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Movies</h2>

                <p>Check the movies section</p>

                <p><a class="btn btn-default" href="/movies/index">Go</a></p>
            </div>
        </div>

    </div>
</div>
