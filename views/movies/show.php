<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $movie app\models\Movies */
/* @var $trailer string */
$this->title = $movie->headline;
$this->params['breadcrumbs'][] = ['label' => 'Movies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $movie->headline;

?>



<div class="site-index">

    <div class="jumbotron">
        <h1><?php echo $movie->headline;?></h1>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <video width="320" height="240" controls>
                    <source src="<?php echo $trailer;?>" type="video/mp4">
                    Your browser does not support the video tag.
                </video>
            </div>
            <div class="col-lg-4">
                <?php echo $movie->body;?>
            </div>
            <div class="col-lg-4">
                <div class="cast">
                    <h2>Cast</h2>
                    <?php
                        foreach ($movie->castingMovies as $cast)
                        {
                            echo '<p>';
                            echo Html::a($cast->casting->name, array('actors/view', 'id'=>$cast->casting->id));
                            echo '</p>';
                        }
                    ?>
                </div>
                <div class="genre">
                    <h2>Genre</h2>
                    <?php
                        foreach ($movie->genresMovies as $genre)
                        {
                            echo '<p>';
                            echo Html::a($genre->genre->type, array('genres/view', 'id'=>$genre->genre->id));
                            echo '</p>';
                        }
                    ?>
                </div>
                <div class="directors">
                    <h2>Directors</h2>
                    <?php
                        foreach ($movie->directorMovies as $director)
                        {
                            echo '<p>';
                            echo Html::a($director->director->name, array('directors/view', 'id'=>$director->director->id));
                            echo '</p>';
                        }
                    ?>
                </div>
            </div>
        </div>

    </div>
</div>