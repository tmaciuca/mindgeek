<?php
use yii\grid\GridView;
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Genres';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-index">
	<h1>Genres</h1>
	<div class="site-about">
		<?php
			echo GridView::widget([
			    'dataProvider' => $this->params['dataProvider'],
			    'columns' => [
			        [
			            'label' => 'ID',
			            'format' => 'raw',
			            'attribute' => 'id',
			            'value' => function ($data) {
			                return Html::a($data['id'], array('genres/view', 'id'=>$data['id']));
			            },
			        ],			    	
			        'type'
			    ]
			]);
		?>
	</div>
</div>

