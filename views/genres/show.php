<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $genre app\models\Genres */

$this->title = $genre->type;
$this->params['breadcrumbs'][] = ['label' => 'Genres', 'url' => ['index']];
$this->params['breadcrumbs'][] = $genre->type;

?>



<div class="site-index">

    <div class="jumbotron">
        <h1><?php echo $genre->type;?></h1>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-6">
                <div class="directors">
                    <h2>Movies</h2>
                    <?php
                        foreach ($genre->movies as $movie)
                        {
                            echo '<p>';
                            echo Html::a($movie->headline, array('movies/view', 'id'=>$movie->id));
                            echo '</p>';
                        }
                    ?>
                </div>
            </div>
        </div>

    </div>
</div>