<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $director app\models\Directors */

$this->title = $director->name;
$this->params['breadcrumbs'][] = ['label' => 'Directors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $director->name;

?>



<div class="site-index">

    <div class="jumbotron">
        <h1><?php echo $director->name;?></h1>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-6">
                <div class="directors">
                    <h2>Movies</h2>
                    <?php
                        foreach ($director->movies as $movie)
                        {
                            echo '<p>';
                            echo Html::a($movie->headline, array('movies/view', 'id'=>$movie->id));
                            echo '</p>';
                        }
                    ?>
                </div>
            </div>
        </div>

    </div>
</div>