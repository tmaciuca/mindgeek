<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $casting app\models\Casting */

$this->title = $casting->name;
$this->params['breadcrumbs'][] = ['label' => 'Actors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $casting->name;

?>



<div class="site-index">

    <div class="jumbotron">
        <h1><?php echo $casting->name;?></h1>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-6">
                <div class="directors">
                    <h2>Movies</h2>
                    <?php
                        foreach ($casting->movies as $movie)
                        {
                            echo '<p>';
                            echo Html::a($movie->headline, array('movies/view', 'id'=>$movie->id));
                            echo '</p>';
                        }
                    ?>
                </div>
            </div>
        </div>

    </div>
</div>