<?php
use yii\grid\GridView;
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Sources';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-index">
	<h1>Manage sources</h1>
	<div class="site-about">
		<?php
			echo GridView::widget([
			    'dataProvider' => $this->params['dataProvider'],
			]);
		?>
	</div>
	<div>
		<?= Html::a('Add new sources', ['/sources/add'], ['class'=>'btn btn-primary']) ?>
</div>

