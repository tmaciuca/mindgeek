STEPS:

git clone https://tmaciuca@bitbucket.org/tmaciuca/mindgeek.git

cd to the project root

composer update

change the username and the password in the projectRoot/config/db.php for mysql

[OPTIONAL] - install php-mysql if not installed

sudo chmod -R 777 projectRoot/runtime

sudo chmod -R 777 projectRoot/web/assets/

sudo mkdir projectRoot/web/movie_assets -> the program itself tries to create the folder but www-data might not have the right to create the directory

sudo chmod 777 projectRoot/web/movie_assets

create an empty database with the name: mindgeek

php yii migrate

php yii serve --port=8888

open http://localhost:8888

---------------------------------------------------------------------

The application will allow users to add sources where items can be downloaded.
From the bootstrap file a source is already added in the table.

The 'Run sources' page will take one unprocessed source from the database and download its items.
Items are also saved in the database and my approach was to have the data normalized as much as I could
This way, even if we have millions of genres, actors, we can easily search by them

The movies section will display the items that are saved from the JSON file



