<?php

return [
    'adminEmail' => 'admin@example.com',
    'allowedImagesType' => array(
    	'image/jpeg',
    	'image/png',
    	'image.gif'
    ),
    'allowedVideoTypes' => array(
    	'video/mp4'
    ),
];
