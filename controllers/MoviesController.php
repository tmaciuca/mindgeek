<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use app\models\Movies;

class MoviesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Movies::find(),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        $this->view->params['dataProvider'] = $dataProvider;
        return $this->render('movies');
    }

    public function actionView($id = '')
    {
        $trailerFileName = '';
        $movie = Movies::find()->where(['id' => (int)$id])->one();
        $trailerPath = Yii::getAlias('@app/web/movie_assets').DIRECTORY_SEPARATOR.$movie->external_id.DIRECTORY_SEPARATOR.'videos';
        $files = scandir($trailerPath);
        // first two elements will always be . and ..
        if (isset($files[2])) {
            $trailerFileName = $files[2];
        }
        $trailerMoviePath = '/movie_assets/'.$movie->external_id.'/videos/'.$trailerFileName;
        return $this->render('show', [
            'movie' => $movie,
            'trailer' => $trailerMoviePath
        ]);
    }
}
