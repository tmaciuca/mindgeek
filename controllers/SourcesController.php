<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Sources;
use app\models\Movies;
use app\components\Helper;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;

class SourcesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Sources::find(),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        $this->view->params['dataProvider'] = $dataProvider;
        return $this->render('sources');
    }

    public function actionAdd()
    {
        $model =  new Sources();
        if ($model->load(Yii::$app->request->post())) {
            $model->user_id = 1;
            $model->processed = 0;
            $model->date_added = date('Y-m-d H:i:s');
            if($model->save()) {
                Yii::$app->session->setFlash('source added');
                return $this->redirect(['sources/index']);   
            } else {
                Yii::$app->session->setFlash($model->getErrors());
                return $this->render('add', [
                   'model' => $model,
                ]);                
            }

 
        } else {
           return $this->render('add', [
               'model' => $model,
           ]);
        }
    }

    public function actionRun()
    {
        /**
         * different approaches
         * normally we should change this in php.ini but giving the fact that this
         * code will be executed on another machine we will set the time limit here
         * 
         * if time++ we can run separate php threads for every item. This should increase the performance
         * */
        set_time_limit(120);
        $movie = new Movies();
        $source = Sources::find()->where(['processed' => 0])->one();
        if($source instanceof Sources && $source->source_url) {
            $url = $source->source_url;
            $result = file_get_contents($url);
            $resultSanitized = Helper::utf8ize($result);
            $convertedResult = Json::decode($resultSanitized);
            if (count($convertedResult)) {
                foreach($convertedResult as $item) {
                    //save assets for every movie
                    $assetSaved = $source->saveItemAssets($item);
                    if ($assetSaved) {
                        $movie->setParamsAndSave($item);

                    }
                }
                $source->processed = 1;
                $source->save();
                Yii::$app->session->setFlash('success', "assets saved");
            }
        } else {
            Yii::$app->session->setFlash('warning', "No unprocessed sources found");
        }
        return $this->redirect(['sources/index']);  
    }
}
