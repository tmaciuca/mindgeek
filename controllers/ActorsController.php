<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use app\models\Casting;

class ActorsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Casting::find(),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        $this->view->params['dataProvider'] = $dataProvider;
        return $this->render('index');
    }

    public function actionView($id = '')
    {
        $casting = Casting::find()->where(['id' => (int)$id])->one();
        return $this->render('show', [
            'casting' => $casting
        ]);
    }
}
