<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use app\models\Genres;

class GenresController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Genres::find(),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        $this->view->params['dataProvider'] = $dataProvider;
        return $this->render('index');
    }

    public function actionView($id = '')
    {
        $genre = Genres::find()->where(['id' => (int)$id])->one();
        return $this->render('show', [
            'genre' => $genre
        ]);
    }
}
