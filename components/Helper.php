<?php
namespace app\components;

class Helper
{
	/**
	 * Method for sanitize json
	 * @param type $mixed 
	 * @return type
	 */
    public static function utf8ize($mixed) {
        if (is_array($mixed)) {
            foreach ($mixed as $key => $value) {
                $mixed[$key] = utf8ize($value);
            }
        } else if (is_string ($mixed)) {
            return utf8_encode($mixed);
        }
        return $mixed;
    }
}