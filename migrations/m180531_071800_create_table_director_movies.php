<?php

use yii\db\Migration;

class m180531_071800_create_table_director_movies extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%director_movies}}', [
            'id' => $this->primaryKey(),
            'movie_id' => $this->integer()->notNull(),
            'director_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('unique_director', '{{%director_movies}}', ['movie_id', 'director_id'], true);
    }

    public function down()
    {
        $this->dropTable('{{%director_movies}}');
    }
}
