<?php

use yii\db\Migration;

class m180531_071800_create_table_genres_movies extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%genres_movies}}', [
            'id' => $this->primaryKey(),
            'movie_id' => $this->integer()->notNull(),
            'genre_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('unique_genre', '{{%genres_movies}}', ['movie_id', 'genre_id'], true);
    }

    public function down()
    {
        $this->dropTable('{{%genres_movies}}');
    }
}
