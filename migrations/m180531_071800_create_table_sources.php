<?php

use yii\db\Migration;

class m180531_071800_create_table_sources extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%sources}}', [
            'id' => $this->primaryKey(),
            'source_url' => $this->string()->notNull(),
            'user_id' => $this->integer(),
            'date_added' => $this->dateTime()->notNull(),
            'processed' => $this->tinyInteger(),
        ], $tableOptions);

        $this->insert('sources', [
            'source_url' => 'https://mgtechtest.blob.core.windows.net/files/showcase.json',
            'user_id' => '1',
            'date_added' => date('Y-m-d H:i:s'),
            'processed' => 0
        ]);

        $this->addForeignKey('fk_casting_id', '{{%casting_movies}}', 'casting_id', '{{%casting}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_movies_id', '{{%casting_movies}}', 'movie_id', '{{%movies}}', 'id', 'RESTRICT', 'RESTRICT');

        $this->addForeignKey('fk_director_id', '{{%director_movies}}', 'director_id', '{{%directors}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_director_movies_id', '{{%director_movies}}', 'movie_id', '{{%movies}}', 'id', 'RESTRICT', 'RESTRICT');

        $this->addForeignKey('fk_genre_id', '{{%genres_movies}}', 'genre_id', '{{%genres}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_genre_movie_id', '{{%genres_movies}}', 'movie_id', '{{%movies}}', 'id', 'RESTRICT', 'RESTRICT');

    }

    public function down()
    {
        $this->dropTable('{{%sources}}');
    }
}
