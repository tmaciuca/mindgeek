<?php

use yii\db\Migration;

class m180531_071800_create_table_casting_movies extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%casting_movies}}', [
            'id' => $this->primaryKey(),
            'movie_id' => $this->integer()->notNull(),
            'casting_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('unique_cast', '{{%casting_movies}}', ['movie_id', 'casting_id'], true);    }

    public function down()
    {
        $this->dropTable('{{%casting_movies}}');
    }
}
