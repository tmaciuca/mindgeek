<?php

use yii\db\Migration;

class m180531_071800_create_table_genres extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%genres}}', [
            'id' => $this->primaryKey(),
            'type' => $this->string(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%genres}}');
    }
}
