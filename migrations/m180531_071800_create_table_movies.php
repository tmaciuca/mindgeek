<?php

use yii\db\Migration;

class m180531_071800_create_table_movies extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%movies}}', [
            'id' => $this->primaryKey(),
            'body' => $this->text(),
            'cert' => $this->string(),
            'class' => $this->string(),
            'duration' => $this->integer(),
            'headline' => $this->string(),
            'external_id' => $this->string(),
            'last_updated' => $this->dateTime(),
            'quote' => $this->text(),
            'rating' => $this->integer(),
            'review_author' => $this->string(),
            'sky_go_id' => $this->string(),
            'sky_go_url' => $this->string(),
            'sum' => $this->string(),
            'synopsys' => $this->text(),
            'url' => $this->string(),
            'viewing_window' => $this->string(),
            'year' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('external_id', '{{%movies}}', 'external_id');
    }

    public function down()
    {
        $this->dropTable('{{%movies}}');
    }
}
