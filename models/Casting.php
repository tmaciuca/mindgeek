<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "casting".
 *
 * @property int $id
 * @property string $name
 *
 * @property CastingMovies[] $castingMovies
 * @property Movies[] $movies
 */
class Casting extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'casting';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCastingMovies()
    {
        return $this->hasMany(CastingMovies::className(), ['casting_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMovies()
    {
        return $this->hasMany(Movies::className(), ['id' => 'movie_id'])->viaTable('casting_movies', ['casting_id' => 'id']);
    }
}
