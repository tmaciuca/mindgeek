<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "director_movies".
 *
 * @property int $id
 * @property int $movie_id
 * @property int $director_id
 *
 * @property Directors $director
 * @property Movies $movie
 */
class DirectorMovies extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'director_movies';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['movie_id', 'director_id'], 'required'],
            [['movie_id', 'director_id'], 'integer'],
            [['movie_id', 'director_id'], 'unique', 'targetAttribute' => ['movie_id', 'director_id']],
            [['director_id'], 'exist', 'skipOnError' => true, 'targetClass' => Directors::className(), 'targetAttribute' => ['director_id' => 'id']],
            [['movie_id'], 'exist', 'skipOnError' => true, 'targetClass' => Movies::className(), 'targetAttribute' => ['movie_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'movie_id' => 'Movie ID',
            'director_id' => 'Director ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDirector()
    {
        return $this->hasOne(Directors::className(), ['id' => 'director_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMovie()
    {
        return $this->hasOne(Movies::className(), ['id' => 'movie_id']);
    }
}
