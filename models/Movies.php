<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "movies".
 *
 * @property int $id
 * @property string $body
 * @property string $cert
 * @property string $class
 * @property int $duration
 * @property string $headline
 * @property string $external_id
 * @property string $last_updated
 * @property string $quote
 * @property int $rating
 * @property string $review_author
 * @property string $sky_go_id
 * @property string $sky_go_url
 * @property string $sum
 * @property string $synopsys
 * @property string $url
 * @property string $viewing_window
 * @property int $year
 *
 * @property CastingMovies[] $castingMovies
 * @property Casting[] $castings
 * @property DirectorMovies[] $directorMovies
 * @property Directors[] $directors
 * @property GenresMovies[] $genresMovies
 * @property Genres[] $genres
 */
class Movies extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'movies';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['body', 'quote', 'synopsys'], 'string'],
            [['duration', 'rating', 'year'], 'integer'],
            [['last_updated'], 'safe'],
            [['cert', 'class'], 'string', 'max' => 64],
            [['headline', 'review_author', 'sky_go_id', 'url', 'viewing_window'], 'string', 'max' => 128],
            [['external_id', 'sky_go_url'], 'string', 'max' => 256],
            [['sum'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'body' => 'Body',
            'cert' => 'Cert',
            'class' => 'Class',
            'duration' => 'Duration',
            'headline' => 'Headline',
            'external_id' => 'External ID',
            'last_updated' => 'Last Updated',
            'quote' => 'Quote',
            'rating' => 'Rating',
            'review_author' => 'Review Author',
            'sky_go_id' => 'Sky Go ID',
            'sky_go_url' => 'Sky Go Url',
            'sum' => 'Sum',
            'synopsys' => 'Synopsys',
            'url' => 'Url',
            'viewing_window' => 'Viewing Window',
            'year' => 'Year',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCastingMovies()
    {
        return $this->hasMany(CastingMovies::className(), ['movie_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCastings()
    {
        return $this->hasMany(Casting::className(), ['id' => 'casting_id'])->viaTable('casting_movies', ['movie_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDirectorMovies()
    {
        return $this->hasMany(DirectorMovies::className(), ['movie_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDirectors()
    {
        return $this->hasMany(Directors::className(), ['id' => 'director_id'])->viaTable('director_movies', ['movie_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGenresMovies()
    {
        return $this->hasMany(GenresMovies::className(), ['movie_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGenres()
    {
        return $this->hasMany(Genres::className(), ['id' => 'genre_id'])->viaTable('genres_movies', ['movie_id' => 'id']);
    }

    /**
     * Instead of this method we can also use a stored procedure for writting data 
     * into all these tables
     * @param type $params 
     * @return type
     */
    public function setParamsAndSave($params)
    {
        $movies = new Movies();
        $movies->body = $params['body'];
        $movies->cert = $params['cert'];
        $movies->class = $params['class'];
        $movies->duration = (int)$params['duration'];
        $movies->headline = $params['headline'];
        $movies->external_id = $params['id'];
        $movies->last_updated = $params['lastUpdated'];
        $movies->quote = $params['quote'] ?? 'no-title-found';
        if (isset($params['rating'])) {
            $movies->rating = (int)$params['rating'];
        } else {
            $movies->rating = 0;
        }
        $movies->review_author = $params['reviewAuthor'] ?? '';
        $movies->sky_go_id = $params['skyGoId'] ?? '';
        $movies->sky_go_url = $params['skyGoUrl'] ?? '';
        $movies->sum = $params['sum'];
        $movies->synopsys = $params['synopsis'];
        $movies->url = $params['url'];
        if (isset($params['viewingWindow'])) {
            $movies->viewing_window = http_build_query($params['viewingWindow'], '', ', '); 
       } else {
            $movies->viewing_window = '';
       }
        
        $movies->year = (int)$params['year'];
        if ($movies->validate()) {
            $movies->save();
            $movieId = $movies->id;
        } else {
            Yii::info('validation error: '.implode(',', $movies->errors), 'error');
        }
        if ($movieId > 0) {
            //write into the casting_movies table
            if (isset($params['cast'])) {
                $castingIds = $this->getCastingIDs($params['cast']);
                foreach ($castingIds as $castId) {
                    $castingMovie = new CastingMovies();
                    $castingMovie->movie_id = $movieId;
                    $castingMovie->casting_id = $castId;
                    try {
                        $castingMovie->save(); 
                    } catch (IntegrityException $e) {
                        Yii::info($e->getMessage(), 'error');
                    }           
                } 
            }

            //write into the director_movies table
            if (isset($params['directors'])) {
                $directorIds = $this->getDirectorIDs($params['directors']);
                foreach ($directorIds as $directorId) {
                    $directorMovies = new DirectorMovies();
                    $directorMovies->movie_id = $movieId;
                    $directorMovies->director_id = $directorId;
                    try {
                        $directorMovies->save(); 
                    } catch (IntegrityException $e) {
                        Yii::info($e->getMessage(), 'error');
                    }           
                }
            }

            //write into the genres_movies table
            if (isset($params['genres'])) {
                $genreIds = $this->getGenresIDs($params['genres']);
                foreach ($genreIds as $genreId) {
                    $genresMovies = new GenresMovies();
                    $genresMovies->movie_id = $movieId;
                    $genresMovies->genre_id = $genreId;
                    try {
                        $genresMovies->save(); 
                    } catch (IntegrityException $e) {
                        Yii::info($e->getMessage(), 'error');
                    }           
                }
            }
        }
    }

    /**
     * Method for adding or fetching the IDs of the casting team
     * Problem here is that if the format of the name is different (eg: last name and first name are switched)
     * we will insert another row
     * One approach is to use soundex algorithm combined maybe with similar_text to make sure that an entity
     * already exists
     * @param array $casting 
     * @return array
     */
    public function getCastingIDs(array $casting)
    {
        $results = array();
        foreach ($casting as $cast) {
            $castingObject = Casting::find()->where(['name' => $cast['name']])->one();
            if ($castingObject instanceOf Casting) {
                $results[] = $castingObject->id;
            } else {
                $castingObject = new Casting();
                $castingObject->name = $cast['name'];
                $castingObject->save();
                $results[] = $castingObject->id;
            }
        }
        return $results;
    }

    /**
     * Because it's very similar with the previous method we can also create
     * a small factory class for this
     * ...when it's time
     * @param array $directors 
     * @return type
     */
    public function getDirectorIDs(array $directors)
    {
        $results = array();
        foreach ($directors as $director) {
            $directorObject = Directors::find()->where(['name' => $director['name']])->one();
            if ($directorObject instanceOf Directors) {
                $results[] = $directorObject->id;
            } else {
                $directorObject = new Directors();
                $directorObject->name = $director['name'];
                $directorObject->save();
                $results[] = $directorObject->id;
            }
        }
        return $results;
    }

    public function getGenresIDs(array $genres)
    {
        $results = array();
        foreach ($genres as $genre) {
            $genreObject = Genres::find()->where(['type' => $genre])->one();
            if ($genreObject instanceOf Genres) {
                $results[] = $genreObject->id;
            } else {
                $genreObject = new Genres();
                $genreObject->type = $genre;
                $genreObject->save();
                $results[] = $genreObject->id;
            }
        }
        return $results;
    }
}
