<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;


class Sources extends ActiveRecord
{
   public function rules()
   {
       return [
           [['source_url','user_id'], 'required'],
           [['source_url'], 'url']
       ];
   }

   public function saveItemAssets(array $item)
   {
   		$sucess = false;
   		$storagePath = Yii::getAlias('@app/web/movie_assets');
		if (isset($item['id'])) {
		    $path = $storagePath.DIRECTORY_SEPARATOR.$item['id'];
		    if (!file_exists($path)) {
		        mkdir($path, 0777, true);
		    }

		    if (isset($item['cardImages']) && count($item['cardImages'])) {
		        $cardImagesPath = $path.DIRECTORY_SEPARATOR.'cardImages';
		        if (!file_exists($cardImagesPath)) {
		            mkdir($cardImagesPath, 0777, true);
		        }
		        foreach ($item['cardImages'] as $key => $value) {
		            $headers = get_headers($value['url'], 1);
		            if (!in_array($headers['Content-Type'], Yii::$app->params['allowedImagesType'])) {
		                continue;
		            }
		            $imageName = explode('/', $value['url']);
		            $imageName = end($imageName);
		            if (!file_exists($cardImagesPath.DIRECTORY_SEPARATOR.$imageName)) {
		                file_put_contents($cardImagesPath.DIRECTORY_SEPARATOR.$imageName, file_get_contents($value['url']));
		            }
		            
		        }
		    }

		    if (isset($item['keyArtImages']) && count($item['keyArtImages'])) {
		        $keyArtImages = $path.DIRECTORY_SEPARATOR.'keyArtImages';
		        if (!file_exists($keyArtImages)) {
		            mkdir($keyArtImages, 0777, true);
		        }
		        foreach ($item['keyArtImages'] as $key => $value) {
		            $headers = get_headers($value['url'], 1);
		            if (!in_array($headers['Content-Type'], Yii::$app->params['allowedImagesType'])) {
		                continue;
		            }
		            $imageName = explode('/', $value['url']);
		            $imageName = end($imageName);
		            if (!file_exists($keyArtImages.DIRECTORY_SEPARATOR.$imageName)) {
		                file_put_contents($keyArtImages.DIRECTORY_SEPARATOR.$imageName, file_get_contents($value['url']));
		            }
		            
		        }
		    }

		    if (isset($item['videos']) && count($item['videos'])) {
		        $videos = $path.DIRECTORY_SEPARATOR.'videos';
		        if (!file_exists($videos)) {
		            mkdir($videos, 0777, true);
		        }
		        foreach ($item['videos'] as $key => $value) {
		            $headers = get_headers($value['url'], 1);
		            if (!in_array($headers['Content-Type'], Yii::$app->params['allowedVideoTypes'])) {
		                continue;
		            }
		            $format = explode('/', $headers['Content-Type'])[1];
		            $videoName = $value['title'].'.'.$format;
		            if (!file_exists($videos.DIRECTORY_SEPARATOR.$videoName)) {
		                file_put_contents($videos.DIRECTORY_SEPARATOR.$videoName, file_get_contents($value['url']));
		            }
		            
		        }
		    }
		    $success = true;
		}
   		return $success;
   }
}