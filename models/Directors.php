<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "directors".
 *
 * @property int $id
 * @property string $name
 *
 * @property DirectorMovies[] $directorMovies
 * @property Movies[] $movies
 */
class Directors extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'directors';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDirectorMovies()
    {
        return $this->hasMany(DirectorMovies::className(), ['director_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMovies()
    {
        return $this->hasMany(Movies::className(), ['id' => 'movie_id'])->viaTable('director_movies', ['director_id' => 'id']);
    }
}
