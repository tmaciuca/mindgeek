<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "casting_movies".
 *
 * @property int $id
 * @property int $movie_id
 * @property int $casting_id
 *
 * @property Casting $casting
 * @property Movies $movie
 */
class CastingMovies extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'casting_movies';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['movie_id', 'casting_id'], 'required'],
            [['movie_id', 'casting_id'], 'integer'],
            [['movie_id', 'casting_id'], 'unique', 'targetAttribute' => ['movie_id', 'casting_id']],
            [['casting_id'], 'exist', 'skipOnError' => true, 'targetClass' => Casting::className(), 'targetAttribute' => ['casting_id' => 'id']],
            [['movie_id'], 'exist', 'skipOnError' => true, 'targetClass' => Movies::className(), 'targetAttribute' => ['movie_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'movie_id' => 'Movie ID',
            'casting_id' => 'Casting ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCasting()
    {
        return $this->hasOne(Casting::className(), ['id' => 'casting_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMovie()
    {
        return $this->hasOne(Movies::className(), ['id' => 'movie_id']);
    }
}
